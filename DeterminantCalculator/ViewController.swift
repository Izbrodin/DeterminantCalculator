import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var resultLabel: UILabel!
    
    @IBOutlet weak var calculationTimeLabel: UILabel!
    
    @IBAction func calculateDeterminantButtonClick(_ sender: UIButton) {
    }
    
    @IBAction func calculateButtonTap(_ sender: UIButton) {
        //Замыкание для заполнения матрицы случайными числами
        let transformCoordinatesToValue:((Int,Int) -> Int64) = {(_ column: Int, _ row: Int) in
            return Int64(arc4random_uniform(6) + 1)
        }
        
        let calculator = DeterminantCalculator.init(dimension: 10, elementConstructor: transformCoordinatesToValue)
        
        let startDate = Date()
        let determinant  = calculator.determinant()
        let calculationTime = Date().timeIntervalSince(startDate)
        
        print("determinant ", determinant)
        resultLabel.text = String(determinant)
        calculationTimeLabel.text = String(calculationTime)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


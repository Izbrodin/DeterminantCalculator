//
//  Matrix.swift
//  DeterminantCalculator

import Foundation

struct Matrix {
    let rows: Int, columns: Int
    var grid: [Int64]
    let indexOutOfRangeMessage = "Index out of range"
    var count: Int { return grid.count }
    init(rows: Int, columns: Int) {
        self.rows = rows
        self.columns = columns
        self.grid = Array(repeating: 0, count: rows * columns)
    }

    mutating func fill(closure elementConstructor: ((Int, Int) -> Int64)) {
        self.grid = (0..<rows * columns).map {
            elementConstructor($0 / rows, $0 % columns)
        }
    }

    func indexIsValid(row: Int, column: Int) -> Bool {
        return row >= 0 && row < rows && column >= 0 && column < columns
    }

    func oneDimensionalIndexIsValid(_ index: Int) -> Bool {
        return index >= 0 && index < rows * columns
    }

    func rowIsValidForMinor(row: Int, rowToRemove: Int) -> Bool {
        return true
    }

    func getMinor(_ rowToRemove: Int, _ columnToRemove: Int) -> Matrix
    {
        var minorMatrix = Matrix(rows: rows - 1, columns: columns - 1)

        let elementsRange = 0..<rows * columns
        for index in elementsRange {
            let row = index / rows
            let column = index % columns
            let rowIndex = row < rowToRemove ? row : row - 1
            let columnIndex = column < columnToRemove ? column : column - 1
            let element = self[row, column]
            if row != rowToRemove && column != columnToRemove {
                minorMatrix[rowIndex, columnIndex] = element
            }
        }
        return minorMatrix
    }

    subscript(row: Int, column: Int) -> Int64 {
        get {
            assert(indexIsValid(row: row, column: column), indexOutOfRangeMessage)
            return grid[(row * columns) + column]
        }

        set {
            assert(indexIsValid(row: row, column: column), indexOutOfRangeMessage)
            grid[(row * columns) + column] = newValue
        }
    }
}

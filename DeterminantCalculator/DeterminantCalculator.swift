import Foundation

class DeterminantCalculator: MatrixTestable
{
    static var author: String {
        return "Izbrodin"
    }

    var matrix: Matrix

    required init(dimension: Int, elementConstructor: ((Int, Int) -> Int64)) {
        matrix = Matrix(rows: dimension, columns: dimension)
        matrix.fill(closure: elementConstructor)
    
        print("Начальная матрица: ")
        printMatrix(matrix)
    }
    
    init(_ matrix: Matrix) {
      self.matrix = matrix
    }

    ///Function for calculating determinant
    func determinant() -> Int64 {
        var det: Int64 = 0
        
        if matrix.rows > 1 {
            for i in 0..<matrix.rows
            {
                let minusOneInPowOfI: Int64 = i % 2 == 0 ? 1 : -1
                let sign = minusOneInPowOfI
                let minor = matrix.getMinor(0, i)
                
                //print("Минор")
                //printMatrix(minor)
                
                let detetrminantOfMinor = DeterminantCalculator.init(minor).determinant()
                det = det + sign * matrix[0, i] * detetrminantOfMinor
            }
        }
        else {
            return matrix[0,0]
        }
        return det
    }
    
    ///Print square matrix function
    func printMatrix(_ matrix: Matrix)
    {
        for index in 0..<matrix.count {
            print(matrix.grid[index], terminator: "    ")

            if index > 0 && (index + 1) % (matrix.rows) == 0 {
                print("")
            }
        }
        print("")
    }
}

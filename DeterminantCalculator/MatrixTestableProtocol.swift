//
//  MatrixProtocol.swift

import Foundation

protocol MatrixTestable {
    /// Author
    static var author: String { get }
    
    /// Matrix constructor
    /// - Parameters:
    ///   - dimension: Matrix size
    ///   - elementConstructor: Closure with input element row and column, which returns the element Int64 value
    init(dimension: Int, elementConstructor: ((_ column: Int, _ row: Int) -> Int64))
    
    /// Determinant calculating function
    //func determinant() -> Int64
    
    func determinant() -> Int64
}
